# Tracking Source

Add the Tracking Source to a url.

## Install

Install the dependency with composer.

```bash
composer require jgp/tracking ^1.1
```

Register the Package in the Microsite Application by editing your `config/app.php`.

```php
<?php

return [
    'aliases' => [
        ...
        'Track' => 'JGP\Tracking\Facades\Track',
    ],
    
    'providers' => [
        ...
        'JGP\Tracking\TrackingServiceProvider',
    ],
];
```

## Usage

Just pass any url you want to track through the `Track::url()` method.

```html
<a href="{{ Track::url($url) }}">Tracked link</a>
```
