<?php

namespace JGP\Tracking;

use CupOfTea\Package\Package;

class Tracking
{
    use Package;
    
    /**
     * Package Name.
     *
     * @const string
     */
    const PACKAGE = 'JGP/Tracking';
    
    /**
     * Package Version.
     *
     * @const string
     */
    const VERSION = '1.1.1';
    
    /**
     * Tracking source from $_GET.
     * 
     * @var string
     */
    protected $source;
    
    /**
     * Default tracking source.
     * 
     * @var string
     */
    protected $defaultSource;
    
    /**
     * Create a new Tracking instance.
     * 
     * @param  array $config
     * @return void
     */
    public function __construct($config)
    {
        $this->defaultSource = $config['default'];
    }
    
    /**
     * Bootstrap the Tracking Service.
     * 
     * @return void
     */
    public function boot()
    {
        if (! empty($_GET['source'])) {
            $this->source = $_COOKIE['_source'] = $_GET['source'];
            setcookie('_source', $this->source, time() + 60 * 60 * 24 * 365.25 * 2);
        }
    }
    
    /**
     * Add the Tracking Source to a url.
     * 
     * @param  string  $url
     * @param  array  $newData
     * @return string
     */
    public function url($url, $newData = []) {
        $parsed = parse_url($url);
        
        if (empty($parsed['query'])) {
            $parsed['query'] = '';
        }
        
        parse_str($parsed['query'], $data);
        
        $data = array_merge($data, $newData);
        
        if (! empty($_COOKIE['_source'])) {
            $data['source'] = $_COOKIE['_source'];
        } else {
            $data['source'] = $this->defaultSource;
        }
        
        $parsed['query'] = http_build_query($data, '', '&', PHP_QUERY_RFC3986);
        
        return http_build_url($parsed);
    }
}
