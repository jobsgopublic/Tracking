<?php

namespace JGP\Tracking;

use Illuminate\Support\ServiceProvider;

class TrackingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application events.
     *
     * @param  \JGP\Tracking\Tracking  $tracking
     * @return void
     */
    public function boot(Tracking $tracking)
    {
        $tracking->boot();
    }
    
    /**
     * Register the Service Provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/tracking.php', 'tracking'
        );
        
        $this->app->singleton(Tracking::class, function ($app) {
            return new Tracking($app['config']['tracking']);
        });
    }
    
    /**
     * Get the Services provided by the Provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            Tracking::class,
        ];
    }
}
