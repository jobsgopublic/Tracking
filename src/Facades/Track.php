<?php

namespace JGP\Tracking\Facades;

use Illuminate\Support\Facades\Facade;
use JGP\Tracking\Tracking;

/**
 * @see \JGP\Tracking\Tracking
 */
class Track extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return Tracking::class;
    }
}
